# -------------------------------------------------------------------------------------------------
# Assignment 1
# Written by Sasa ZHANG (student ID: 25117151)
# For COMP 472 Lecture Section AA & Lab Section AK-X ------ Summer 2021
# -------------------------------------------------------------------------------------------------

import sys
import math
import numpy as np
from heapq import heappush, heappop, heapify
from point import Point


class AStar:
    def __init__(self, starting_point, ending_point, player_map):
        self.open_list = []
        self.close_list = []
        self.path_list = []
        self.optimal_path_list = []
        self.goal_list = []

        self.starting_point = starting_point
        self.ending_point = ending_point
        self.player_map = player_map

        self.has_path = False

        # Used in is_accessible() to make 2 playing ground grids inaccessible.
        self.infinity = 999999

    def is_in_open_list(self, point_a):
        """
        Check whether point_a is inside the open_list.
        :param point_a: a point waiting to be checked.
        :return: True or False
        """
        for pi in self.open_list:
            if point_a == pi:
                return True

        return False

    def is_in_close_list(self, point_a):
        """
        Check whether point_a is inside the close_list.
        :param point_a: a point waiting to be checked.
        :return: True or False
        """
        for pi in self.close_list:
            if point_a == pi:
                return True

        return False

    def push2_open_list(self, point_a, point_bx, point_by):
        """
        Calculate the g_cost for all neighbours' of point_a and push them into the open_list.
        :param point_a: a point on the map.
        :param point_bx: neighbour point's x value.
        :param point_by: neighbour point's y value.
        """
        tmp_num = self.get_pnum_inlist(point_bx, point_by)
        if tmp_num is not None:
            cost_current = self.calculate_cost(point_a, self.player_map.intersections_list[tmp_num])
            index_current = self.get_pnum_inlist(point_a.x_coordinate, point_a.y_coordinate)

            if cost_current is not None:
                if (self.player_map.role == 'role_c' and
                    self.is_accessible(self.player_map.intersections_list[index_current],
                                       self.player_map.intersections_list[tmp_num])) or \
                        self.player_map.role == 'role_v':
                    pass
                else:
                    self.player_map.intersections_list[tmp_num].g_cost = self.infinity

                # If g_cost is definite and point_b is not within the open_list and close_list,
                # push point_b to the open_list.
                if (self.player_map.intersections_list[tmp_num].g_cost < self.infinity) and \
                        not self.is_in_close_list(self.player_map.intersections_list[tmp_num]) and \
                        not self.is_in_open_list(self.player_map.intersections_list[tmp_num]):
                    # Update g_cost, h_cost, f_cost and parent
                    self.player_map.intersections_list[tmp_num].parent = \
                        self.player_map.intersections_list[index_current]
                    self.player_map.intersections_list[tmp_num].g_cost = \
                        np.round(cost_current+self.player_map.intersections_list[tmp_num].parent.g_cost, 1)
                    self.player_map.intersections_list[tmp_num].h_cost = \
                        np.round(self.calculate_cost_h(self.player_map.intersections_list[tmp_num]), 1)
                    self.player_map.intersections_list[tmp_num].f_cost = \
                        np.round(self.player_map.intersections_list[tmp_num].h_cost+
                                 self.player_map.intersections_list[tmp_num].g_cost, 1)

                    # Use 'heappush' to push into the open_list
                    heappush(self.open_list, self.player_map.intersections_list[tmp_num])

                # If point_b is already within the open_list, compare the f_cost
                # Store the smaller f_cost, g_cost and its immediate parent
                if self.is_in_open_list(self.player_map.intersections_list[tmp_num]):
                    g_cost_pt = cost_current + self.player_map.intersections_list[index_current].g_cost
                    h_cost_pt = self.calculate_cost_h(self.player_map.intersections_list[tmp_num])
                    f_cost_pt = g_cost_pt + h_cost_pt

                    # Compare the f_cost to save the smaller one
                    if f_cost_pt < self.player_map.intersections_list[tmp_num].f_cost:
                        print(f'......Point({self.player_map.intersections_list[tmp_num].x_coordinate}, '
                              f'{self.player_map.intersections_list[tmp_num].y_coordinate}): '
                              f'the old f cost {self.player_map.intersections_list[tmp_num].f_cost} '
                              f'is replaced by {f_cost_pt}......')

                        # Replace and update the f_cost, g_cost and parent.
                        self.player_map.intersections_list[tmp_num].parent = \
                            self.player_map.intersections_list[index_current]
                        self.player_map.intersections_list[tmp_num].g_cost = np.round(g_cost_pt, 1)
                        self.player_map.intersections_list[tmp_num].h_cost = np.round(h_cost_pt, 1)
                        self.player_map.intersections_list[tmp_num].f_cost = np.round(f_cost_pt, 1)

            else:
                self.player_map.intersections_list[tmp_num].g_cost = self.infinity
        else:
            return

    def create_a_goal_list(self):
        """
        Store a list of goal points into 'goal_list'.
        """
        tmp_goal = 0
        for pi in range(len(self.player_map.intersections_list)):
            # Store a point as a goal point if it is inside a quarantine place - for 'role_c'.
            if self.player_map.role == 'role_c':
                if self.is_quarantine_place(self.player_map.intersections_list[pi]):
                    self.goal_list.append(self.player_map.intersections_list[pi])
                    tmp_goal = tmp_goal + 1
            # Store a point as a goal point if it is inside a vaccine spot - for 'role_v'.
            elif self.player_map.role == 'role_v':
                if self.is_vaccine_spot(self.player_map.intersections_list[pi]):
                    self.goal_list.append(self.player_map.intersections_list[pi])
                    tmp_goal = tmp_goal + 1
        print(f'......There are {tmp_goal} goal points in the following astar search......')

    def is_a_valid_point(self, point_obj):
        """
        Check the validity of a point on a map.
        :param point_obj: another point object on a map.
        :return: True or False
        """
        if isinstance(point_obj, Point):
            if 0 <= point_obj.x_coordinate <= self.player_map.x_lim and \
                    0 <= point_obj.y_coordinate <= self.player_map.y_lim:
                return True
        print(f'......Point({point_obj.x_coordinate}, {point_obj.y_coordinate}) is not valid......')
        return False

    def is_accessible(self, point_a, point_b):
        """
        Check whether the edge is among 2 playing grounds.
        :param point_a: a point on the map.
        :param point_b: another point on the map.
        :return: True or False
        """
        # Find out the common grids among point_a and point_b.
        list_intersection = list(point_a.belonging_grids.intersection(point_b.belonging_grids))

        # If point_a and point_b have exactly 2 common grids.
        if len(list_intersection) == 2:
            if self.player_map.grids_list[list_intersection[0] - 1].type == 'pg' and \
                    self.player_map.grids_list[list_intersection[1] - 1].type == 'pg':
                print(f'......Inaccessible between point({point_a.x_coordinate}, {point_a.y_coordinate}) and '
                      f'point({point_b.x_coordinate}, {point_b.y_coordinate})......')
                return False

        return True

    def transform_a_point(self, point_obj):
        """
        Transform a point into a top-right corner point on the map.
        :param point_obj: another point object on the map.
        """
        if self.is_a_valid_point(point_obj) and self.is_a_valid_transformation(point_obj):

            # If the point is within a grid, transform it to the nearest top-right point of the grid.
            for grid_i in self.player_map.grids_list:

                if grid_i.x_min < point_obj.x_coordinate < grid_i.x_max and \
                        grid_i.y_min < point_obj.y_coordinate < grid_i.y_max:

                    if self.player_map.role == 'role_c':
                        # top-right corner on the map.
                        x_tmp, y_tmp = grid_i.get_top_right_coordinate()

                    elif self.player_map.role == 'role_v':
                        # left-bottom corner on the map.
                        x_tmp, y_tmp = grid_i.get_bottom_left_coordinate()

                    else:
                        print(f'......No player role by that type......')

                    # Check whether the point is on the intersections of the map.
                    for pi in self.player_map.intersections_list:
                        if x_tmp == pi.x_coordinate and y_tmp == pi.y_coordinate:
                            print(f'Point({point_obj.x_coordinate}, {point_obj.y_coordinate}) is '
                                  f'transformed into ({pi.x_coordinate}, {pi.y_coordinate})......')
                            point_obj.x_coordinate = pi.x_coordinate
                            point_obj.y_coordinate = pi.y_coordinate
                            point_obj.belonging_grids = pi.belonging_grids
                            return

            # print(f'......Point being checked is: ({point_obj.x_coordinate},
            # {point_obj.y_coordinate})......') # display

            # Do a multiple by 10 to solve the floating point can't be compared directly.
            # If the point is one of the intersections on the map.
            for pi in self.player_map.intersections_list:
                if point_obj.x_coordinate == pi.x_coordinate and point_obj.y_coordinate == pi.y_coordinate:
                    print(f'......Point({pi.x_coordinate}, {pi.y_coordinate}) is one of the intersections......')
                    point_obj.x_coordinate = pi.x_coordinate
                    point_obj.y_coordinate = pi.y_coordinate
                    point_obj.belonging_grids = pi.belonging_grids
                    return

            # If the point is on a row boundary
            if np.round(math.fmod(point_obj.y_coordinate*10, self.player_map.grid_width*10), 1) == 0.0 and \
                    np.round(math.fmod(point_obj.x_coordinate*10, self.player_map.grid_length*10), 1) != 0.0:
                if self.player_map.role == 'role_c':
                    # Make sure the point is not on the edge between 2 playing grounds.
                    if not self.is_accessible(self.player_map.intersections_list[self.get_pnum_inlist(point_obj.x_coordinate,point_obj.y_coordinate)],
                                              self.player_map.intersections_list[self.get_pnum_inlist(np.round(point_obj.x_coordinate - self.player_map.grid_length, 1), point_obj.y_coordinate)]):
                        print(f'......Invalid point as a point can not go through 2 playing grounds......')
                        sys.exit(0)
                    # Move the point right.
                    point_obj.x_coordinate = np.round((point_obj.x_coordinate*10+
                                                       (self.player_map.grid_length*10-np.round(math.fmod(point_obj.x_coordinate*10, self.player_map.grid_length*10), 1)))/10, 1)

                if self.player_map.role == 'role_v':
                    # round the point's x value: move left
                    point_obj.x_coordinate = np.round((point_obj.x_coordinate*10-
                                                       np.round(math.fmod(point_obj.x_coordinate*10, self.player_map.grid_length*10), 1))/10, 1)

            # If the point is on a column boundary
            if np.round(math.fmod(point_obj.y_coordinate*10, self.player_map.grid_width*10), 1) != 0.0 and \
                    np.round(math.fmod(point_obj.x_coordinate*10, self.player_map.grid_length*10), 1) == 0.0:
                if self.player_map.role == 'role_c':
                    # Make sure the point is not on the edge between 2 playing grounds.
                    if not self.is_accessible(self.player_map.intersections_list[self.get_pnum_inlist(point_obj.x_coordinate, point_obj.y_coordinate)],
                                              self.player_map.intersections_list[self.get_pnum_inlist(point_obj.x_coordinate, np.round(point_obj.y_coordinate-self.player_map.grid_width, 1))]):
                        print(f'......Invalid point as a point can not go through 2 playing grounds......')
                        sys.exit(0)
                    point_obj.y_coordinate = np.round((point_obj.y_coordinate*10+
                                                       (self.player_map.grid_width*10-np.round(math.fmod(point_obj.y_coordinate*10, self.player_map.grid_width * 10), 1)))/10, 1)
                if self.player_map.role == 'role_v':
                    # Move the point down.
                    point_obj.y_coordinate = np.round((point_obj.y_coordinate*10-np.round(math.fmod(point_obj.y_coordinate*10, self.player_map.grid_width*10), 1))/10, 1)

        else:
            print(f'......Point({point_obj.x_coordinate}, {point_obj.y_coordinate}) can not be transformed......')

    def is_a_valid_transformation(self, point_obj):
        """
        Check whether the point needs to be transformed.
        :param point_obj: another point object on the map.
        :return: True or False
        """
        # If the point is one of the intersection points on the map, then no need to do the transformation.
        for pi in self.player_map.intersections_list:
            if pi == point_obj:
                print(f'......Intersection({point_obj.x_coordinate}, {point_obj.y_coordinate}) '
                      f'does not need to be transformed......')
                return False

        return True

    def is_quarantine_place(self, point_a):
        """
        Verify whether a point is already in a quarantine place.
        :param point_a: a point waiting to be verified.
        :return: True or False
        """
        # First to check if point_a is the ending point.
        if point_a == self.ending_point:
            print(f'......The ending point({point_a.x_coordinate}, {point_a.y_coordinate}) '
                  f'is not in any of quarantine places......')

        # Extract the index from the list of intersections on the map.
        index_i = self.get_pnum_inlist(point_a.x_coordinate, point_a.y_coordinate)

        # Verify whether one of point_a's belonging grids is the quarantine place.
        for gi in self.player_map.intersections_list[index_i].belonging_grids:
            if self.player_map.grids_list[gi-1].type == 'qp':
                return True

        return False

    def is_vaccine_spot(self, point_a):
        """
        Verify whether a point is already in a vaccine spot.
        :param point_a: a point waiting to be verified.
        :return: True or False
        """
        # First to check if point_a is the ending point.
        if point_a == self.ending_point:
            print(f'......The ending point({point_a.x_coordinate}, {point_a.y_coordinate}) '
                  f'is not in any of vaccine spots......')

        # Extract the index from the list of intersections on the map.
        index_i = self.get_pnum_inlist(point_a.x_coordinate, point_a.y_coordinate)

        # Verify whether one of point_a's belonging grids is the vaccine spot.
        for gi in self.player_map.intersections_list[index_i].belonging_grids:
            if self.player_map.grids_list[gi - 1].type == 'vs':
                return True

        return False

    def get_pnum_inlist(self, point_ax, point_ay):
        """
        Get the points' number within the intersections_list.
        :param point_ax: x value of point_a
        :param point_ay: y value of point_a
        :return: pt_index
        """
        for pt_index, pi in enumerate(self.player_map.intersections_list):
            if point_ax == pi.x_coordinate and point_ay == pi.y_coordinate:
                return pt_index

        return None

    def calculate_cost(self, point_a, point_b):
        """
        Calculate the total cost between 2 points on the map.
        :param point_a: one point on the map.
        :param point_b: another point on the map.
        :return: calculated total cost
        """
        # Find common grids between 2 points.
        cnum = self.get_pnum_inlist(point_a.x_coordinate, point_a.y_coordinate)
        intersection_list = list(self.player_map.intersections_list[cnum].belonging_grids.intersection(point_b.belonging_grids))

        if len(intersection_list) == 2:
            return np.round((self.player_map.grids_list[intersection_list[0]-1].cost+self.player_map.grids_list[intersection_list[1]-1].cost)/2, 1)

        elif len(intersection_list) == 1:
            # If 2 points are on a diagonal line within a same grid, use the larger value of the square root.
            if self.player_map.role == 'role_v':
                # Direction: north_west
                if np.round(point_a.x_coordinate - point_b.x_coordinate, 1) == 0.2 and \
                        np.round(point_a.y_coordinate - point_b.y_coordinate, 1) == -0.1:
                    g1 = self.calculate_cost(point_a, self.player_map.intersections_list[self.get_pnum_inlist(np.round(point_a.x_coordinate - 0.2, 1), point_a.y_coordinate)])
                    g2 = self.calculate_cost(self.player_map.intersections_list[self.get_pnum_inlist(np.round(point_a.x_coordinate-0.2, 1), point_a.y_coordinate)], point_b)
                    g3 = self.calculate_cost(point_a, self.player_map.intersections_list[self.get_pnum_inlist(point_a.x_coordinate, np.round(point_a.y_coordinate+0.1, 1))])
                    g4 = self.calculate_cost(self.player_map.intersections_list[self.get_pnum_inlist(point_a.x_coordinate,np.round(point_a.y_coordinate+0.1, 1))], point_b)

                    if g1 is not None and g2 is not None and g3 is not None and g4 is not None:
                        return max(np.round(math.sqrt(g1**2 + g2**2), 1), np.round(math.sqrt(g3**2 + g4**2), 1))
                    else:
                        return None

                # Direction: north_east
                if np.round(point_a.x_coordinate - point_b.x_coordinate, 1) == -0.2 and \
                        np.round(point_a.y_coordinate - point_b.y_coordinate, 1) == -0.1:
                    g1 = self.calculate_cost(point_a, self.player_map.intersections_list[self.get_pnum_inlist(np.round(point_a.x_coordinate+0.2, 1), point_a.y_coordinate)])
                    g2 = self.calculate_cost(self.player_map.intersections_list[self.get_pnum_inlist(np.round(point_a.x_coordinate+0.2, 1), point_a.y_coordinate)], point_b)
                    g3 = self.calculate_cost(point_a, self.player_map.intersections_list[self.get_pnum_inlist(point_a.x_coordinate, np.round(point_a.y_coordinate+0.1, 1))])
                    g4 = self.calculate_cost(self.player_map.intersections_list[self.get_pnum_inlist(point_a.x_coordinate, np.round(point_a.y_coordinate+0.1, 1))], point_b)

                    if g1 is not None and g2 is not None and g3 is not None and g4 is not None:
                        return max(np.round(math.sqrt(g1**2 + g2**2), 1), np.round(math.sqrt(g3**2 + g4**2), 1))
                    else:
                        return None

                # Direction: south_west
                if np.round(point_a.x_coordinate - point_b.x_coordinate,1) == 0.2 and np.round(point_a.y_coordinate - point_b.y_coordinate,1) == 0.1:
                    g1 = self.calculate_cost(point_a, self.player_map.intersections_list[self.get_pnum_inlist(np.round(point_a.x_coordinate-0.2, 1), point_a.y_coordinate)])
                    g2 = self.calculate_cost(self.player_map.intersections_list[self.get_pnum_inlist(np.round(point_a.x_coordinate-0.2, 1), point_a.y_coordinate)], point_b)
                    g3 = self.calculate_cost(point_a, self.player_map.intersections_list[self.get_pnum_inlist(point_a.x_coordinate, np.round(point_a.y_coordinate-0.1, 1))])
                    g4 = self.calculate_cost(self.player_map.intersections_list[self.get_pnum_inlist(point_a.x_coordinate, np.round(point_a.y_coordinate-0.1, 1))], point_b)

                    if g1 is not None and g2 is not None and g3 is not None and g4 is not None:
                        return max(np.round(math.sqrt(g1**2 + g2**2), 1), np.round(math.sqrt(g3**2 + g4**2), 1))
                    else:
                        return None

                # Direction: south_east
                if np.round(point_a.x_coordinate - point_b.x_coordinate,1) == -0.2 and np.round(point_a.y_coordinate - point_b.y_coordinate,1) == 0.1:
                    g1 = self.calculate_cost(point_a, self.player_map.intersections_list[self.get_pnum_inlist(np.round(point_a.x_coordinate+0.2, 1), point_a.y_coordinate)])
                    g2 = self.calculate_cost(self.player_map.intersections_list[self.get_pnum_inlist(np.round(point_a.x_coordinate+0.2, 1), point_a.y_coordinate)], point_b)
                    g3 = self.calculate_cost(point_a, self.player_map.intersections_list[self.get_pnum_inlist(point_a.x_coordinate, np.round(point_a.y_coordinate-0.1, 1))])
                    g4 = self.calculate_cost(self.player_map.intersections_list[self.get_pnum_inlist(point_a.x_coordinate, np.round(point_a.y_coordinate-0.1, 1))], point_b)

                    if g1 is not None and g2 is not None and g3 is not None and g4 is not None:
                        return max(np.round(math.sqrt(g1**2 + g2**2), 1), np.round(math.sqrt(g3**2 + g4**2), 1))
                    else:
                        return None

            return self.player_map.grids_list[intersection_list[0]-1].cost

        else:
            print(f'......No intersected grids......')
            return None

    def calculate_cost_h(self, point_a):
        """
        Calculate h_cost.
        :param point_a: a point on the map.
        :return: tmp_h_cost
        """
        # Role C: "Manhattan distance" - abs(point.x_coordinate - goal.x_coordinate) + abs(point.y_coordinate - goal.y_coordinate)
        # Role V: "Chebyshev distance" - max(|point.x_coordinate-goal.x_coordinate|, |point.y_coordinate - goal.y_coordinate|)
        if self.player_map.role == 'role_c':
        # Divide (point.x_coordinate - goal.x_coordinate) by 2 to make the grid as a square.
        # Make the points move in 4 equal steps no matter in which directions.
            tmp_h_cost = np.round(10*(abs((point_a.x_coordinate - self.goal_point.x_coordinate)/2)+
                                      abs(point_a.y_coordinate - self.goal_point.y_coordinate)), 1)
        elif self.player_map.role == 'role_v':
            # For the diagonal directions, divide (point.x_coordinate - goal.x_coordinate)/2
            # Make the points move in 8 equal steps no matter in which directions.
            tmp_h_cost = 10*max(np.round(abs((point_a.x_coordinate-self.goal_point.x_coordinate)/2), 1),
                                np.round(abs(point_a.y_coordinate - self.goal_point.y_coordinate), 1))
        return tmp_h_cost

    def create_a_path(self, found_a_path, f_cost):
        """
        Create a path and save it in the path_list.
        :param found_a_path: a founded path
        :param f_cost: f_cost of the path
        """
        tmp_path = []

        for path_i in found_a_path:
            tmp_path.append(path_i.x_coordinate)
            tmp_path.append(path_i.y_coordinate)

        heappush(self.optimal_path_list, self.OptimalPath(np.array(tmp_path).reshape(-1, 2), f_cost))

    def get_optimal_path(self):
        """
        Get the optimal path.
        :return: the optimal path
        """
        return self.optimal_path

    def find_an_optimal_path(self):
        """
        Find the optimal path and display the message to the player.
        """
        self.has_path = True

        # Utilize the priority queue to get the optimal path with the least f_cost
        self.optimal_path_pt = heappop(self.optimal_path_list)
        self.optimal_path = self.optimal_path_pt.found_a_path
        self.optimal_path_total_cost = np.round(self.optimal_path_pt.total_cost, 1)
        print(f'\n======== Total Cost: {self.optimal_path_total_cost} ========\n')
        # print(f'======== Optimal Path ========\n{self.optimal_path}\n'
        #       f'\n======== Total Cost: {self.optimal_path_total_cost} ========\n')

    def get_a_path(self):
        """
        Find a path and f_cost
        """
        print(f'==============================')
        # case: arrive in the end point
        if len(self.close_list) > 0:
            print(f'......A path is found......')

            # Add the arrived point
            self.path_list.append(self.close_list[-1])

            # Find all parents of the current arrived point
            for i in range(len(self.close_list)):
                if self.path_list[i] == self.starting_point:
                    break
                else:
                    self.path_list.append(self.path_list[-1].parent)
            self.path_list.reverse()

            # f_cost - h_cost to set the arrived point's h_cost = 0
            self.path_list[-1].f_cost = self.path_list[-1].f_cost - self.path_list[-1].h_cost

            # Display the f_cost of the last point within the close_list.
            print(f'f_cost = {self.path_list[-1].f_cost}')

            for path_i in self.path_list:
                print(f'[{path_i.x_coordinate, path_i.y_coordinate}]', end=' ')

            # Save the founded path in the path_list.
            print('', end='\n')
            self.create_a_path(self.path_list, self.path_list[-1].f_cost)
        else:
            print(f'......No path is found...the close list is empty......')
        print(f'==============================')

    def start_search(self, goal_point):
        """
        Start the astar algorithm to search for optimal path.
        :param goal_point: a destination point
        """
        # Check whether the ending point is pointing to a quarantine place.
        if (self.player_map.role == 'role_c' and self.is_quarantine_place(goal_point))\
                or (self.player_map.role == 'role_v' and self.is_vaccine_spot(goal_point)):
            print(f'==============================')
            print(f'......Running astar to find the optimal path......')

            # Sort the open_list.
            heapify(self.open_list)

            pq_index = self.get_pnum_inlist(self.starting_point.x_coordinate, self.starting_point.y_coordinate)
            heappush(self.open_list, self.player_map.intersections_list[pq_index])

            while self.open_list:

                # Pop a smallest f_cost point.
                current_sp = heappop(self.open_list)

                # Add a point to the close_list.
                self.close_list.append(current_sp)

                # Verify the player's role.
                if self.player_map.role == 'role_c' and \
                        (current_sp == goal_point or self.is_quarantine_place(current_sp)):

                    # Verify if the current point is already reach to the goal point
                    print(f'......A path is found......')
                    self.get_a_path()
                    break

                if self.player_map.role == 'role_v' and \
                        (current_sp == goal_point or self.is_vaccine_spot(current_sp)):

                    # Verify if the current point is already reach to the goal point
                    print(f'......A path is found......')
                    self.get_a_path()
                    break

                # Calculate the g_cost of its children and push them into the open_list.
                print(f'==============================')

                # Sequence for role_c: up, down, left, right
                self.push2_open_list(current_sp, current_sp.x_coordinate,
                                     np.round(current_sp.y_coordinate+self.player_map.grid_width, 1))
                self.push2_open_list(current_sp, current_sp.x_coordinate,
                                     np.round(current_sp.y_coordinate-self.player_map.grid_width, 1))
                self.push2_open_list(current_sp,
                                     np.round(current_sp.x_coordinate-self.player_map.grid_length, 1),
                                     current_sp.y_coordinate)
                self.push2_open_list(current_sp,
                                     np.round(current_sp.x_coordinate+self.player_map.grid_length, 1),
                                     current_sp.y_coordinate)
                print(f'......Completed all 4 directions......')

                # role_v has 4 additional diagonal directions
                if self.player_map.role == 'role_v':
                    # Sequence for role_v: north_east, north_west, south_east, south_west
                    self.push2_open_list(current_sp, np.round(current_sp.x_coordinate+self.player_map.grid_length, 1),
                                         np.round(current_sp.y_coordinate + self.player_map.grid_width, 1))
                    self.push2_open_list(current_sp, np.round(current_sp.x_coordinate-self.player_map.grid_length, 1),
                                         np.round(current_sp.y_coordinate + self.player_map.grid_width, 1))
                    self.push2_open_list(current_sp, np.round(current_sp.x_coordinate+self.player_map.grid_length, 1),
                                         np.round(current_sp.y_coordinate - self.player_map.grid_width, 1))
                    self.push2_open_list(current_sp, np.round(current_sp.x_coordinate-self.player_map.grid_length, 1),
                                         np.round(current_sp.y_coordinate - self.player_map.grid_width, 1))
                    print(f'......Completed all 8 directions......')
                print(f'==============================')

    def run(self):
        """
        Run the astar algorithm.
        :return:
        """
        # Get all goal points.
        self.create_a_goal_list()

        # Make the starting point and ending point are both valid.
        if self.is_a_valid_point(self.starting_point) and self.is_a_valid_point(self.ending_point):
            print(f'......Both starting point and ending point are validated......')

            # Transform the starting point and ending point, if necessary.
            self.transform_a_point(self.starting_point)
            self.transform_a_point(self.ending_point)

            print(f'......Starting point is: ({self.starting_point.x_coordinate}, {self.starting_point.y_coordinate})\n'
                  f'Ending point is: ({self.ending_point.x_coordinate}, {self.ending_point.y_coordinate})')

            # Check whether the starting point is already within a quarantine place or a vaccine spot.
            if self.player_map.role == 'role_c':
                if self.is_quarantine_place(self.starting_point):
                    print(f'......The starting point({self.starting_point.x_coordinate}, '
                          f'{self.starting_point.y_coordinate}) is already in a quarantine place...'
                          f'no need to run the astar to search for the optimal path......')
                    return
            elif self.player_map.role == 'role_v':
                if self.is_vaccine_spot(self.starting_point):
                    print(f'......The starting point({self.starting_point.x_coordinate}, '
                          f'{self.starting_point.y_coordinate}) is already in a vaccine spot...'
                          f'no need to run the astar to search for the optimal path......')
                    return
            else:
                print(f'......No player role by that type......')
            print(f'......Validated starting point and the astar is about to run......')

            heapify(self.optimal_path_list)

            # Rank goal points
            for goal_i in self.goal_list:
                # Calculate h_cost.
                self.goal_point = goal_i
                self.start_search(goal_i)

                # Reset all parameters of astar algorithm for the next round.
                self.reset_search()

            self.find_an_optimal_path()
            # print(f'......The optimal path searching is done......')
            print(f'======== Optimal Path ========\n')

        else:
            print(f'......Oops...something went wrong...please try again......')

    def reset_search(self):
        """
        Reset the astar search.
        """
        # Reset the f_cost, g_cost, h_cost.
        for i in range(len(self.player_map.intersections_list)):
            self.player_map.intersections_list[i].f_cost = 0.0
            self.player_map.intersections_list[i].g_cost = 0.0
            self.player_map.intersections_list[i].h_cost = 0.0
            self.player_map.intersections_list[i].parent = None
        # Reset path_list, open_list and close_list.
        self.path_list.clear()
        self.open_list.clear()
        self.close_list.clear()

    # Inner class
    class OptimalPath:
        def __init__(self, found_a_path, total_cost):
            self.found_a_path = found_a_path # store the path
            self.total_cost = total_cost # total cost from start to goal point

        def __lt__(self, other_path):
            """
            override the 'less than' method which is used to compare the 'f_cost' between 2 paths on the map.
            :param other_path: another path on the map
            :return: True or False
            """
            if self.total_cost < other_path.total_cost:
                return True

            return False
