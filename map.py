# -------------------------------------------------------------------------------------------------
# Assignment 1
# Written by Sasa ZHANG (student ID: 25117151)
# For COMP 472 Lecture Section AA & Lab Section AK-X ------ Summer 2021
# -------------------------------------------------------------------------------------------------

import numpy as np
from grid import Grid
from point import Point


class Map:
    def __init__(self, row_map, col_map):
        self.grid_length = 0.2
        self.grid_width = 0.1
        self.row_map = row_map
        self.col_map = col_map

        self.x_lim = np.round(self.col_map * self.grid_length, 1)
        self.y_lim = np.round(self.row_map * self.grid_width, 1)

        # List stores all grids on the map.
        self.grids_list = []

        # List stores all intersection points on the map.
        self.intersections_list = []

        # Role type of the player; default player is 'Role C'.
        self.role = 'role_c'

    def create_intersections(self):
        """
        Create all intersection points on the map.
        """
        for xi in self.x_ticks:
            for yi in self.y_ticks:
                self.intersections_list.append(Point(xi, yi))
        # print(f'......The number of intersections is: {len(self.intersections_list)}......')  # display

        for pi in self.intersections_list:
            for gi in self.grids_list:
                x_bottom_left, y_bottom_left = gi.get_bottom_left_coordinate()
                x_bottom_right, y_bottom_right = gi.get_bottom_right_coordinate()
                x_top_right, y_top_right = gi.get_top_right_coordinate()
                x_top_left, y_top_left = gi.get_top_left_coordinate()
                if (pi.x_coordinate == x_bottom_left and pi.y_coordinate == y_bottom_left) or (
                        pi.x_coordinate == x_top_right and pi.y_coordinate == y_top_right) \
                        or (pi.x_coordinate == x_top_left and pi.y_coordinate == y_top_left) or (
                        pi.x_coordinate == x_bottom_right and pi.y_coordinate == y_bottom_right):
                    pi.belonging_grids.add(gi.grid_num)
        # print(f'......All intersections find its belonging grids......\n')  # display

    def create_grids(self):
        """
        Create all grids on the map.
        """
        self.x_point = np.round(self.x_ticks[0:-1], 1)
        self.y_point = np.round(np.flip(self.y_ticks[0:-1]), 1)

        i = 1
        for yi in self.y_point:
            for xi in self.x_point:
                self.grids_list.append(Grid(xi, yi, i, self.grid_length, self.grid_width, self.role))
                i = i + 1
        # print(f'......Total number of grids is: {len(self.grids_list)}......')  # display

    def get_ticks(self):
        """
        Set ticks both for x-axis and y-axis.
        :return: x_ticks, y_ticks
        """
        self.x_ticks = np.round(np.linspace(0, self.x_lim, self.col_map+1), 1)
        self.y_ticks = np.round(np.linspace(0, self.y_lim, self.row_map+1), 1)

        return self.x_ticks, self.y_ticks
