# -------------------------------------------------------------------------------------------------
# Assignment 1
# Written by Sasa ZHANG (student ID: 25117151)
# For COMP 472 Lecture Section AA & Lab Section AK-X ------ Summer 2021
# -------------------------------------------------------------------------------------------------


class Point:
    def __init__(self, x_coordinate, y_coordinate):
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate
        self.parent = None

        # A set contains the number of belonging grids.
        self.belonging_grids = set()

        self.f_cost = 0.0
        self.g_cost = 0.0
        self.h_cost = 0.0

    def __eq__(self, point_obj):
        """
        override the equal method.
        :param point_obj: another point on the map
        :return: True or False
        """
        if self.x_coordinate == point_obj.x_coordinate and self.y_coordinate == point_obj.y_coordinate:
            return True

        return False

    def __lt__(self, point_obj):
        """
        override the 'less than' method which is used to compare the 'f_cost' between 2 points on the map.
        :param point_obj: another point on the map
        :return: True or False
        """
        if self.f_cost < point_obj.f_cost:
            return True

        return False
