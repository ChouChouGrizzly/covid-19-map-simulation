# -------------------------------------------------------------------------------------------------
# Assignment 1
# Written by Sasa ZHANG (student ID: 25117151)
# For COMP 472 Lecture Section AA & Lab Section AK-X ------ Summer 2021
# -------------------------------------------------------------------------------------------------


Running environment:
- Python 3.8.8
- Built-in Libraries:
    1. os
    2. sys
    3. matplotlib
    4. math
    5. numpy
    6. heapq

List of files:
- covid-19_map_simulation.py
- astar.py
- map.py
- grid.py
- point.py

Generated directory:
- covid_maps
    1. map_initial.png
    2. map_path_role_c.png
    3. map_path_role_v.png

Running instructions:
1. Open up a terminal
2. "cd" to the directory of "A1_25117151"
3. Enter "python covid-19_map_simulation.py" to run the program, sometimes need to enter "python3 covid-19_map_simulation.py" to run the program
4. Follow the prompt messages of the program and you are all set.
