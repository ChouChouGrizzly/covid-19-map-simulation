# -------------------------------------------------------------------------------------------------
# Assignment 1
# Written by Sasa ZHANG (student ID: 25117151)
# For COMP 472 Lecture Section AA & Lab Section AK-X ------ Summer 2021
# -------------------------------------------------------------------------------------------------

import os
import sys
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from map import Map
from point import Point
from astar import AStar


def main():
    # Create a welcome message for user.
    print('\n============================== Welcome to Covid-19 Map Simulation =============================')

    # Prompt the user that the map simulation is about to start.
    print("Let's get started......")

    # ----------------- Step 1: Decide the map's size by requiring the user's inputs. -----------------
    print('\n--------------------------------------- Step 1 ------------------------------------------------')
    # Ask the user to enter their desired number of rows for their covid-19 map.
    # Verify the user's input to make sure it is a positive integer.
    while True:
        try:
            row_map = int(input('Enter your desired positive number of rows for the Covid-19 Map: '))
        except ValueError:
            continue
        else:
            if row_map <= 0:
                continue
            else:
                print(f'......You entered number of rows for the Covid-19 Map is: {row_map}......\n')
                break

    # Ask the user to enter their desired number of columns for their covid-19 map.
    # Verify the user's input to make sure it is a positive integer.
    while True:
        try:
            col_map = int(input('Enter your desired positive number of columns for the Covid-19 Map: '))
        except ValueError:
            continue
        else:
            if col_map <= 0:
                continue
            else:
                print(f'......You entered number of columns for the Covid-19 Map is: {col_map}......')
                break

    # Display the user's size of map.
    print(f'\n......The size of your Covid-19 Map is: {row_map} by {col_map}......')

    # Start to set up the map.
    fig, ax = plt.subplots(figsize=(14, 7))

    # Create a Map object.
    player_map = Map(row_map, col_map)

    # Set the range and scale for both x-axis and y-axis.
    ax.set_xlim(0, player_map.x_lim)
    ax.set_ylim(0, player_map.y_lim)
    x_ticks, y_ticks = player_map.get_ticks()
    ax.set_xticks(x_ticks)
    ax.set_yticks(y_ticks)

    # ----------------- Step 2: Decide the role of player: Role C and Role V. -----------------
    print('\n--------------------------------------- Step 2 ------------------------------------------------')
    # Ask the user to choose their desired type of player
    # as Role C and Role V have different associated cost when generating the optimal path.
    while True:
        try:
            player_role = int(
                input(f'Enter your desired type of role (1: Role C; 2: Role V): '))
        except ValueError:
            continue
        else:
            if player_role == 1:
                player_map.role = 'role_c'
                role_title = 'Covid-19 Confirmed Patients (Role C)'
                print(f'\n......You chose "Role C" to play......\n')
                break
            elif player_role == 2:
                player_map.role = 'role_v'
                role_title = 'Healthy People to Get Vaccine (Role V)'
                print(f'\n......You chose "Role V" to play......\n')
                break
            else:
                print(f'\n......Invalid role type, please re-do your choose......\n')

    # Set all grids on the map.
    player_map.create_grids()

    # Create all intersections on the map.
    player_map.create_intersections()

    # -------------------------- Step 3: Assign the grids to 3 different places -----------------------
    print('\n--------------------------------------- Step 3 ------------------------------------------------')
    num_of_grids = row_map * col_map  # num_grids

    # Prompt user to assign the grids to quarantine place.
    while True:
        try:
            # user input different types of the grids
            quarantine_assign = input(f'Assign a list of your desired grids between 1 to {num_of_grids} '
                                      f'(separated by space) to "quarantine place": ').split()
            if all(1 <= int(q) <= num_of_grids for q in quarantine_assign):
                quarantine_grids = [int(quarantine_assign[i]) for i in range(len(quarantine_assign))]
                break
        except ValueError:
            print("......Invalid selection of quarantine grids......")

    # Prompt user to assign the grids to vaccine spot.
    while True:
        try:
            # user input different types of the grids
            vaccine_assign = input(f'\nAssign a list of your desired grids between 1 to {num_of_grids} '
                                   f'(separated by space) to "vaccine spot": ').split()
            if all(1 <= int(v) <= num_of_grids for v in vaccine_assign):
                vaccine_grids = [int(vaccine_assign[i]) for i in range(len(vaccine_assign))]
                break
        except ValueError:
            print("......Invalid selection of vaccine grids......")

    # Prompt user to assign the grids to playing ground.
    while True:
        try:
            # user input different types of the grids
            playing_assign = input(f'\nAssign a list of your desired grids between 1 to {num_of_grids} '
                                   f'(separated by space) to "playing ground": ').split()
            if all(1 <= int(p) <= num_of_grids for p in playing_assign):
                playing_grids = [int(playing_assign[i]) for i in range(len(playing_assign))]
                break
        except ValueError:
            print("......Invalid selection of playing grids......")

    # Set the type for each grid.
    for qp_i in quarantine_grids:
        player_map.grids_list[qp_i - 1].set_grid_type('qp')
    # Grid type of 'vaccine spot'.
    for vs_i in vaccine_grids:
        player_map.grids_list[vs_i - 1].set_grid_type('vs')
    # Grid type of 'playing ground'.
    for pg_i in playing_grids:
        player_map.grids_list[pg_i - 1].set_grid_type('pg')

    # Assign different color for different grid type.
    for grid in player_map.grids_list:
        if grid.type == 'qp':
            grid_rect = mpatches.Rectangle(grid.get_bottom_left_coordinate(), player_map.grid_length,
                                           player_map.grid_width, fc='yellowgreen', ec='blue')
            ax.add_patch(grid_rect)
            ax.text(grid.x_coordinate + player_map.grid_length / 2, grid.y_coordinate + player_map.grid_width / 2,
                    'QP', alpha=1, color='k', fontsize=14)
        elif grid.type == 'vs':
            grid_rect = mpatches.Rectangle(grid.get_bottom_left_coordinate(), player_map.grid_length,
                                           player_map.grid_width, fc='beige', ec='blue')
            ax.add_patch(grid_rect)
            ax.text(grid.x_coordinate + player_map.grid_length / 2, grid.y_coordinate + player_map.grid_width / 2,
                    'VS', alpha=1, color='k', fontsize=14)
        elif grid.type == 'pg':
            grid_rect = mpatches.Rectangle(grid.get_bottom_left_coordinate(), player_map.grid_length,
                                           player_map.grid_width, fc='plum', ec='blue')
            ax.add_patch(grid_rect)
            ax.text(grid.x_coordinate + player_map.grid_length / 2, grid.y_coordinate + player_map.grid_width / 2,
                    'PG', alpha=1, color='k', fontsize=14)
        elif grid.type == 'number':
            grid_rect = mpatches.Rectangle(grid.get_bottom_left_coordinate(), player_map.grid_length,
                                           player_map.grid_width, fc='silver', ec='blue')
            ax.add_patch(grid_rect)
            ax.text(grid.x_coordinate + player_map.grid_length / 2, grid.y_coordinate + player_map.grid_width / 2,
                    str(grid.grid_num), alpha=1, color='k', fontsize=14)

    # Set the legend on the map.
    grid_qp = mpatches.Patch(label='quarantine place', color='yellowgreen')
    grid_vs = mpatches.Patch(label='vaccine spot', color='beige')
    grid_pg = mpatches.Patch(label='playing ground', color='plum')
    grid_num = mpatches.Patch(label='unassigned grid', color='silver')
    plt.legend(loc='upper left', bbox_to_anchor=(1.05, 1.0), handles=[grid_qp, grid_vs, grid_pg, grid_num])

    ax.grid(True)
    plt.tight_layout()

    # Equalize the scale of x-axis and y-axis
    plt.gca().set_aspect('equal', adjustable='box')

    # Save the map.
    ax.set_title(f'The Covid-19 Map Simulation - {role_title}')
    if not os.path.exists('./covid_maps'):
        os.makedirs('covid_maps')
    fig.savefig(r'./covid_maps/map_initial.png')
    print(f'......The initial map is generated and saved in "covid_maps" directory......\n')

    # -------------------------- Step 4: Decide the starting and ending points -----------------------
    print('\n--------------------------------------- Step 4 ------------------------------------------------')
    while True:
        try:
            starting_x, starting_y = map(float, input(f'Enter your desired starting point: '
                                                      f'x between [0, {player_map.x_lim}] and '
                                                      f'y between [0, {player_map.y_lim}] (separated by space): ').split())

            if 0 <= starting_x <= player_map.x_lim and 0 <= starting_y <= player_map.y_lim:
                print(f'......Valid starting points on the map......')
                break
            else:
                print(
                    f'......The starting point you just entered is not within the map range...Please try again......\n')
                continue
        except ValueError:
            print(f'......Invalid starting points on the map......')

    while True:
        try:
            ending_x, ending_y = map(float, input(f'\nEnter your desired ending point: '
                                                  f'x between [0, {player_map.x_lim}] and '
                                                  f'y between [0, {player_map.y_lim}] (separated by space): ').split())

            if 0 <= ending_x <= player_map.x_lim and 0 <= ending_y <= player_map.y_lim:
                print(f'......Valid ending points on the map......\n......The ending point will still '
                      f'be assigned according to your selected player role by the program......\n')
                break
            else:
                print(f'......The ending point you just entered is not within the map range...Please try again......\n')
                continue
        except ValueError:
            print(f'......Invalid ending points on the map......')

    # -------------------------- Step 5: Start A-Star Algorithm Search -----------------------
    print('\n--------------------------------------- Step 5 ------------------------------------------------')
    # Create both starting point and ending point object.
    starting_point = Point(starting_x, starting_y)
    ending_point = Point(ending_x, ending_y)

    # Start astar search.
    player_astar = AStar(starting_point, ending_point, player_map)
    player_astar.run()

    if player_astar.has_path:
        player_path = player_astar.get_optimal_path()
        print(player_path)
        ax.plot(*zip(*player_path), markersize=11, marker='o', color='green',
                linestyle='solid', linewidth=6, label='a path')

        # Display starting point and ending point.
        ax.plot(*zip(player_path[0]), markersize=11, marker='o', color='blue')
        ax.plot(*zip(player_path[-1]), markersize=11, marker='o', color='green')

        # Generate new legend after the optimal path is found.
        path_start = mpatches.Patch(color='blue', label='starting point')
        path_end = mpatches.Patch(color='green', label='ending point')
        map_path = mpatches.Patch(color='green', label='optimal path')
        ax.legend(loc='upper left', bbox_to_anchor=(1.05, 1.0), handles=[grid_qp, grid_vs, grid_pg,
                                                                         path_start, path_end, map_path])

        if player_map.role == 'role_c':
            fig.savefig(r'./covid_maps/map_path_role_c.png')
            print(f'\n......The optimal path map for Role C is generated and saved in "covid_maps" directory......\n')
        elif player_map.role == 'role_v':
            fig.savefig(r'./covid_maps/map_path_role_v.png')
            print(f'\n......The optimal path map for Role V is generated and saved in "covid_maps" directory......\n')
        plt.show()
    else:
        print(f'......No optimal path is found......')
        sys.exit(0)


if __name__ == '__main__':
    main()
