# -------------------------------------------------------------------------------------------------
# Assignment 1
# Written by Sasa ZHANG (student ID: 25117151)
# For COMP 472 Lecture Section AA & Lab Section AK-X ------ Summer 2021
# -------------------------------------------------------------------------------------------------

import numpy as np


class Grid:
    def __init__(self, x_coordinate, y_coordinate, grid_num, grid_len, grid_wid, player_role):
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate
        self.grid_num = grid_num
        self.grid_length = grid_len
        self.grid_width = grid_wid
        self.role = player_role

        self.x_min = self.x_coordinate
        self.x_max = np.round(self.x_coordinate+self.grid_length, 1)
        self.y_min = self.y_coordinate
        self.y_max = np.round(self.y_coordinate+self.grid_width, 1)

        if player_role == 'role_c':
            self.cost = 1.0
        elif player_role == 'role_v':
            self.cost = 2.0
        else:
            print("......No player role by that type......")

        # Default type of grid
        self.type = "number"

    def get_top_right_coordinate(self):
        """
        Get the top-right coordinates of the grid.
        :return: x_top_right, y_top_right
        """
        self.x_top_right = np.round(self.x_coordinate+self.grid_length, 1)
        self.y_top_right = np.round(self.y_coordinate+self.grid_width, 1)

        return self.x_top_right, self.y_top_right

    def get_bottom_left_coordinate(self):
        """
        Get the bottom-left coordinate of the grid.
        :return: x_coordinate, y_coordinate
        """
        return self.x_coordinate, self.y_coordinate

    def get_top_left_coordinate(self):
        """
        Get the top-left coordinate of the grid.
        :return: x_top_left, y_top_left
        """
        self.x_top_left = np.round(self.x_coordinate, 1)
        self.y_top_left = np.round(self.y_coordinate+self.grid_width, 1)

        return self.x_top_left, self.y_top_left

    def get_bottom_right_coordinate(self):
        """
        Get the bottom-right coordinate of the grid.
        :return: x_bottom_right, y_bottom_right
        """
        self.x_bottom_right = np.round(self.x_coordinate+self.grid_length, 1)
        self.y_bottom_right = np.round(self.y_coordinate, 1)
        return self.x_bottom_right, self.y_bottom_right

    def set_grid_type(self, grid_type):
        """
        Set the grid type based on the role type and assign the pre-determined cost for each grid type.
        :param grid_type: the type of the grid
        """
        if grid_type == 'qp':
            self.type = 'qp'
            if self.role == 'role_c':
                self.cost = 0.0
            elif self.role == 'role_v':
                self.cost = 3.0
        elif grid_type == 'vs':
            self.type = 'vs'
            if self.role == 'role_c':
                self.cost = 2.0
            elif self.role == 'role_v':
                self.cost = 0.0
        elif grid_type == 'pg':
            self.type = 'pg'
            if self.role == 'role_c':
                self.cost = 3.0
            elif self.role == 'role_v':
                self.cost = 1.0
        else:
            if self.role == 'role_c':
                self.cost = 1.0
            elif self.role == 'role_v':
                self.cost = 2.0
            print('......No such player role type...Will be assigned to default "number" type......')
